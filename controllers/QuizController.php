<?php


namespace app\controllers;

use app\models\Quiz;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class QuizController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Quiz models.
     * @return mixed
     */
    public function actionCreate()
    {
        return $this->render('create');
    }

    public function actionRename($id = null)
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            if ($data && $data['id'] && $data['name']) {
                $quiz = Quiz::findOne($data['id']);
                $quiz->name = $data['name'];
                if ($quiz->save()) {
                    Yii::$app->session->setFlash('success', "Quiz successfully renamed");
                    return true;
                } else {
                    Yii::$app->session->setFlash('danger', "Error saving quiz");
                    return false;
                }
            } else {
                Yii::$app->session->setFlash('danger', "Error receiving quiz data");
                return false;
            }
        } else {
            return $this->render(
                'rename',
                [
                    'model' => $this->findModel($id),
                ]
            );
        }
    }

    public function actionSave()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            if ($data) {
                $quiz = new Quiz();
                $quiz->name = $data['name'];
                if ($quiz->save()) {
                    Yii::$app->session->setFlash('success', "Quiz successfully created");
                    return $quiz->getPrimaryKey();
                } else {
                    Yii::$app->session->setFlash('danger', "Error saving quiz");
                    return false;
                }
            } else {
                Yii::$app->session->setFlash('danger', "Error saving quiz");
                return false;
            }
        } else {
            return $this->redirect('/');
        }
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider(
            [
                'query' => Quiz::find(),
            ]
        );

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
            ]
        );
    }

    public function actionView($id)
    {
        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
            ]
        );
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionUpdate($id)
    {
        return $this->render(
            'update',
            [
                'model' => $this->findModel($id),
            ]
        );
    }

    public function actionSaveUpdatedQuiz()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            if ($data && $data['id'] && $data['quiz'] && $data['name']) {
                $quiz = Quiz::findOne($data['id']);
                $quiz->quiz_data = $data['quiz'];
                $quiz->name = $data['name'];
                if ($quiz->save()) {
                    Yii::$app->session->setFlash('success', "Quiz successfully updated");
                    return true;
                } else {
                    Yii::$app->session->setFlash('danger', "Error saving quiz");
                    return false;
                }
            } else {
                Yii::$app->session->setFlash('danger', "Error receiving quiz data");
                return false;
            }
        } else {
            return $this->redirect('/');
        }
    }

    public function actionSaveResults()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            if ($data) {
                $_SESSION['user_results'] = $data;
                return true;
            } else {
                Yii::$app->session->setFlash('danger', "Error receiving quiz results");
                return false;
            }
        } else {
            return $this->redirect('/');
        }
    }

    public function actionResults()
    {
        if (isset($_SESSION['user_results'])) {
            return $this->render(
                'results',
                [
                    'model' => Quiz::findOne($_SESSION['user_results']['quiz_id']),
                ]
            );
        } else {
            return $this->redirect('/');
        }
    }

    protected function findModel($id)
    {
        if (($model = Quiz::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}