<?php

namespace app\controllers;

use Yii;
use app\models\Projects;
use app\models\Orders;
use app\models\People;
use app\models\ProjectSettings;
use app\models\UserSettings;
use app\models\Store;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use dektrium\user\models\User;
use yii\db\Expression;
// use Datetime;
/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Projects models.
     * @return mixed
     */
    public function actionIndex()
    {
		
		$this->layout = "dark_layout";
		
		
        $dataProvider = new ActiveDataProvider([
            'query' => Projects::find(),
        ]);
		
		$model = new Projects();
		
        if ($model->load(Yii::$app->request->post())) {
			
			
			$admin_user_id = Yii::$app->user->getId();
			
			
			$model->user_id = $admin_user_id;
			$model->save(false);
				
				
            return $this->redirect(['index', 'id' => $model->id]);
        
		} else {
	        return $this->render('index', [
	            'dataProvider' => $dataProvider,
	        ]);
        }
		
		

 
    }

    /**
     * Displays a single Projects model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($project_id)
    {
		
		$this->layout = "dark_layout";
		
		
        return $this->render('view', [
            'model' => $this->findModel($project_id),
        ]);
    }
	
	
    public function actionOverview($project_id)
    {
        if ( Yii::$app->user->isGuest ){
            return $this->redirect('/user/login');
        }
        
        $session = Yii::$app->session;
        if ($session->has('last_login_user')) {
            $todayTo = date('yy-m-d H:i:s', $session['last_login_user']);
        } else {
            $session['last_login_user'] = time();
            $todayTo = date('yy-m-d H:i:s', $session['last_login_user']);
            $userModel = User::findOne(['id' => Yii::$app->user->id]);
            $userModel->updateAttributes(['last_login_at' => $session['last_login_user']]);
        }
              
		$cookies = Yii::$app->response->cookies;
					// add a new cookie to the response to be sent
					$cookies->add(new \yii\web\Cookie([
					    'name' => 'last_project_id',
					    'value' => $project_id,
					]));
					
                   $date = Yii::$app->request->get('date');
            
        
        //get minimal date from database for Datapicker
        $minDateForChart = Orders::getMinDateByProjectID($project_id);

        //get maximal date from database for Datapicker
        $maxDateForChart = Orders::getMaxDateProjectID($project_id);
                    
        //get all dates in format '04/20/2020 00:00:00'
        $todayFrom = date('yy-m-d 00:00:00');
        $yesterday = date('yy-m-d 24:00:00', strtotime( '-1 days' ));
        $past7days = date('yy-m-d 24:00:00', strtotime( '-7 days' ));
        $past14days = date('yy-m-d 24:00:00', strtotime( '-14 days' ));
        $past30days = date('yy-m-d 24:00:00', strtotime( '-30 days' ));

        // get data today Revenue, Orders and People
        $revenueOrdersToday = Orders::getDataByDatesRevenueAndOrders($project_id, $todayFrom, $todayTo);
        $peopleToday = People::getDataByDatesRevenueAndOrders($project_id, $todayFrom, $todayTo);

        //get data during 7 days
        $revenue7PastDays = Orders::getSumArrayByKey(Orders::getDataByDatesRevenueAndOrders($project_id, $past7days, $yesterday), 'revenue');
        $orders7PastDays = Orders::getSumArrayByKey(Orders::getDataByDatesRevenueAndOrders($project_id, $past7days, $yesterday), 'orders');
        $people7PastDays = People::getSumArrayByKey(People::getDataByDatesRevenueAndOrders($project_id, $past7days, $yesterday), 'people');

        //get data during 14 days
        $revenue14PastDays = Orders::getSumArrayByKey(Orders::getDataByDatesRevenueAndOrders($project_id, $past14days, $yesterday), 'revenue');
        $orders14PastDays = Orders::getSumArrayByKey(Orders::getDataByDatesRevenueAndOrders($project_id, $past14days, $yesterday), 'orders');
        $people14PastDays = People::getSumArrayByKey(People::getDataByDatesRevenueAndOrders($project_id, $past14days, $yesterday), 'people');
        
        //get data during 30 days
        $revenue30PastDays = Orders::getSumArrayByKey(Orders::getDataByDatesRevenueAndOrders($project_id, $past30days, $yesterday), 'revenue');
        $orders30PastDays = Orders::getSumArrayByKey(Orders::getDataByDatesRevenueAndOrders($project_id, $past30days, $yesterday), 'orders');
        $people30PastDays = People::getSumArrayByKey(People::getDataByDatesRevenueAndOrders($project_id, $past30days, $yesterday), 'people');
                    
					//$revenue_array = Projects::revenue($project_id, $date);
					//$sevendays = $revenue_array['purchase_conversion_value_7'];
					
					//echo"$sevendays <br /><br />";
					
					//print_r($revenue_array);
					
					//die();
							
        return $this->render('overview', [
            'model'                 => $this->findModel($project_id),
            'minDate'               => date('yy-m-d', $minDateForChart),
            'maxDate'               => date('yy-m-d', $maxDateForChart),
            'revenueOrdersToday'    => $revenueOrdersToday,
            'peopleToday'           => $peopleToday,
            'revenue7PastDays'       => $revenue7PastDays,
            'orders7PastDays'        => $orders7PastDays,
            'revenue14PastDays'      => $revenue14PastDays,
            'orders14PastDays'       => $orders14PastDays,
            'revenue30PastDays'      => $revenue30PastDays,
            'orders30PastDays'       => $orders30PastDays,
            'people7PastDays'        => $people7PastDays,
            'people14PastDays'       => $people14PastDays,
            'people30PastDays'       => $people30PastDays
        ]);
    }

    public function actionUpdateChart()
	{
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();

            //check today date
            if($data['to'] == date('yy-m-d 24:00:00')) {
                $session = Yii::$app->session;
                if ($session->has('last_login_user')) {
                    $dateTo = date('yy-m-d H:i:s', $session['last_login_user']);
                }
            } else {
                $dateTo = $data['to'];
            }

            //get data about Revenue and Orders from DB 
            $dataChart = Orders::getDataByDatesRevenueAndOrders($data['project_id'], $data['from'], $dateTo);

            // create Array of Data for Chart
            $chartArray = array();
            array_push($chartArray, array('Date', 'Revenue', 'Orders'));
            if (!$dataChart) {
                array_push($chartArray, array('', 0, 0));
            } else {
                foreach($dataChart as $item) {
                    array_push($chartArray, array(date('m-d-yy',strtotime($item['ndate'])), doubleval($item['revenue']), doubleval($item['orders'])));
                }
            }

            return json_encode($chartArray);
        }
    }
	
	

    /**
     * Creates a new Projects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Projects();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


	
	
    public function actionNew()
    {
        
		$admin_user_id = Yii::$app->user->getId();
		
		$usersettings = UserSettings::findOne(['user_id' => $admin_user_id]);
		
		$superadmin_id = $usersettings->superadmin_id;
		
		
		$user = User::findOne($admin_user_id);
		
		$model = new Projects();
		$model->user_id = $superadmin_id;
		$model->to_delete = 0;
		$model->title = "Untitled";
	
		
	 $email = $user->email;
	 
	 $email_array = explode('@',$email);
	 
	 $firstpart = $email_array[0];
	 
	 $email_fixed = str_replace('.','', $firstpart);
	 
	 $rand = rand(1,900);
	 
	 $username = $email_fixed.$rand;
	 
	 	
		
			$model->save(false);
			
			
		$ps = new ProjectSettings();
		$ps->project_id = $model->id;
		
		$ps->superadmin_id = $superadmin_id;
		$ps->username = $username;
		
		$ps->id = $model->id;
		
		$ps->save(false);
		
		$project_id = $ps->project_id;
		
		Store::initial_template(1, $project_id);
			
			
		return $this->redirect(['project-settings/onboarding', 'project_id' => $ps->project_id]);
        
    }
	
	
	
	
    /**
     * Updates an existing Projects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Projects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }



    public function actionRemove($id)
    {
        $model = $this->findModel($id);
		
		$model->to_delete = 1;
		
		$model->save(false);

        return $this->redirect(['index']);
    }
	
	
	
    /**
     * Finds the Projects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Projects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Projects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
