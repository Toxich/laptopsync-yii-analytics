<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Request;
use DOMDocument;

class FeedsController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionParse()
    {
        return $this->render('parse');
    }

    public function actionGetByUrl()
    {
        $url = Yii::$app->request->post('url');
        try {
            $url_contents = file_get_contents($url);
        } catch (\Exception $exception) {
            return json_encode(['result' => false, 'message' => 'Page was not found']);
        }
        $site = new DOMDocument();
        libxml_use_internal_errors(true);
        $site->loadHtml($url_contents);
        $links = $site->getElementsByTagName('head')->item(0)->getElementsByTagName('link');
        $rssUrl = '';
        if ($links && count($links) >= 1) {
            foreach ($links as $link) {
                $checkType = false;
                foreach ($link->attributes as $attribute) {
                    if ($attribute->name == 'type' && $attribute->value == 'application/rss+xml') {
                        $checkType = true;
                    }
                }
                if ($checkType) {
                    foreach ($link->attributes as $attribute) {
                        if ($attribute->name == 'href') {
                            $rssUrl = $attribute->value;
                        }
                    }
                    break;
                }
            }
        }
        if ($rssUrl !== '') {
            exec('cd /var/www/html/ && php yii parse '.$rssUrl.' > /dev/null 2>/dev/null &');
            return json_encode(['result' => true, 'message' => 'Posts parsing successfully started!']);
        } else {
            return json_encode(['result' => false, 'message' => 'Rss feed url was not found on site']);
        }
    }

}
