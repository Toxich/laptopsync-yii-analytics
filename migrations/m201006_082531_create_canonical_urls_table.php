<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%canonical_urls}}`.
 */
class m201006_082531_create_canonical_urls_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%canonical_urls}}', [
            'id' => $this->primaryKey(),
            'canonical_url' => $this->string(),
            'member_post_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%canonical_urls}}');
    }
}
