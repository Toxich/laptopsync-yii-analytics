<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\PostUrl;
use yii\console\Controller;
use app\models\Post;
use app\models\Category;
use DOMDocument;

class ParseController extends Controller
{
    public function actionIndex($url)
    {
        $feed = simplexml_load_file($url);
        foreach($feed->channel->item as $story){
            $content = '';
            $imageUrl = '';
            $contentXml = null;
            $imageContent = null;
            if ($story->children('content', true)->encoded) {
                $content =  $story->children('content', true)->encoded;
                $contentXml = new DOMDocument();
                libxml_use_internal_errors(true);
                $contentXml->loadHtml($content);
                if($contentXml->getElementsByTagName('figure')->item(0) && $contentXml->getElementsByTagName('figure')->item(0)->getElementsByTagName('img')->item(0)) {
                    $imageContent = $contentXml->getElementsByTagName('figure')->item(0)->getElementsByTagName('img')->item(0);
                } else if ($contentXml->getElementsByTagName('img')->item(0)) {
                    $imageContent = $contentXml->getElementsByTagName('img')->item(0);
                }
                if ($imageContent) {
                    foreach ($imageContent->attributes as $attribute) {
                        if ($attribute->name == 'src') {
                            $imageUrl = $attribute->value;
                        }
                    }
                }
            } elseif ($story->description) {
                $content = $story->description;
            }
            $post = new Post();
            $post->title = $story->title;
            $post->content = $content;
            $post->featured_image = $imageUrl;
            $post->date = \Yii::$app->formatter->asDate($story->pubDate, 'yyyy-MM-dd');
            $post->save();
            Category::create($story->category, $post->id);
            PostUrl::create($story->link, $post->id);
        }
        echo 'success' . PHP_EOL;
    }
}
