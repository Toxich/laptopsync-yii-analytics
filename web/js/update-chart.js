google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    let from = $('#start').val() + ' 00:00:00';
    let to = $('#end').val() + ' 24:00:00';
    let project = $("#project").val();
    $("#pickDate").attr("disabled", true);
        $.ajax({
            url: "/projects/update-chart",
            type: "POST",
            dataType: 'json',
            data: {
                from: from,
                to: to,
                'project_id' : project,
                _csrf: yii.getCsrfToken()
            },
            success: function (data) {

                $("#pickDate").removeAttr("disabled");
                var data = google.visualization.arrayToDataTable(data);

                var options = {
                  legend: { position: 'bottom' }
                };
              
                var chart = new google.visualization.LineChart(document.getElementById('chart'));
              
                chart.draw(data, options);
            },
            error: function (data) {
                alert("Error request")
            }
            
        });

$(document).ready(function(){   
    $('#pickDate').on('click', function(){
        drawChart();
    })    
});
}
