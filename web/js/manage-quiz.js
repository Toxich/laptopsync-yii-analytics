//Variables
let question_type = '';
//Functions
function array_move(arr, old_index, new_index) {
    if (new_index >= arr.length) {
        var k = new_index - arr.length + 1;
        while (k--) {
            arr.push(undefined);
        }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr;
}

if (quiz_data) {
    let quiz_content = document.getElementById('questions-list');
    new Sortable(quiz_content, {
        animation: 150,
        ghostClass: 'blue-background-class',
        onEnd: function (/**Event*/evt) {
            if (evt.oldIndex !== evt.newIndex) {
                quiz_data = array_move(quiz_data, evt.oldIndex, evt.newIndex);
                saveUpdates();
            }
        }
    });
}

function saveUpdates() {
    const urlParams = new URLSearchParams(window.location.search);
    const id = urlParams.get('id');
    $.ajax({
        url: "/quiz/save-updated-quiz",
        type: "POST",
        dataType: 'json',
        data: {
            id: id,
            name: quiz_name,
            quiz: quiz_data
        },
        success: function () {
        },
        error: function (data) {
            console.log(data);
            alert("ERROR");
        }
    });
}

function getLastAnswer(question_index) {
    let last = 0;
    if (quiz_data[question_index].answers && quiz_data[question_index].answers.length >= 1) {
        last = quiz_data[question_index].answers[quiz_data[question_index].answers.length - 1].id;
    }
    last++;
    return last;
}

function formQuestionsList() {
    let questions_content = '';
    quiz_data.forEach((question) => {
        questions_content += questionsListElement(question.id, question.question);
    });
    document.getElementById('questions-list').innerHTML = questions_content;
}

function displayQuestion(id = '') {
    if (!tinymce.activeEditor) {
        let question;
        let data = id.split('-');
        question = jQuery.grep(quiz_data, function (question) {
            if (data.length < 3) {
                return question;
            } else {
                if (question.id == data[2]) {
                    return question;
                }
            }
        });
        if (question.length !== 0) {
            let answer_content = '';
            let add_button_content = '';
            if (question[0].type === 'open') {
                if (!question[0].answers || question[0].answers.length === 0) {
                    add_button_content = buttonAddAnswer(question[0].id);
                } else {
                    answer_content = openAnswerContent(question[0].id, question[0].answers[0].id, question[0].answers[0].answer);
                }
            } else if (question[0].type === 'test') {
                if (!question[0].answers || question[0].answers.length === 0) {
                    add_button_content = buttonAddAnswer(question[0].id);
                } else {
                    let button_correct_style = '';
                    question[0].answers.forEach((answer) => {
                        button_correct_style = answer.id == question[0].correct_answer ? '' : 'btn-outline';
                        answer_content += testAnswerContent(question[0].id, answer.id, answer.answer, button_correct_style);
                    });
                    add_button_content = buttonAddAnswer(question[0].id);
                }
            }
            document.getElementById('quiz-content').innerHTML = showQuestionContent(question[0].id, question[0].question, answer_content, add_button_content);
        }
    } else {
        alert("Please finish editing and save before opening a new editor");
    }
}

function setAnswerType(id) {
    if (id === 'open-answer' && question_type === '') {
        question_type = 'open';
        document.getElementById(id).classList.remove('btn-outline')
    } else if (id === 'test-answer' && question_type === '') {
        question_type = 'test';
        document.getElementById(id).classList.remove('btn-outline')
    } else if (question_type === 'test' && id === 'open-answer') {
        question_type = 'open';
        document.getElementById(id).classList.remove('btn-outline');
        document.getElementById('test-answer').classList.add('btn-outline')
    } else {
        question_type = 'test';
        document.getElementById(id).classList.remove('btn-outline');
        document.getElementById('open-answer').classList.add('btn-outline')
    }
}

function addQuestion() {
    if (!tinymce.activeEditor) {
        if (document.getElementById('quiz-content').innerHTML !== '') {
            document.getElementById('quiz-content').innerHTML = '';
        }
        document.getElementById('tinymce-area').innerHTML += createQuestionTinyMCEArea(questions);
        initiateMCE();
        document.getElementById("save-text").onclick = function () {
            if (question_type === '') {
                alert('Please choose answer type first');
            } else {
                if (quiz_data.length > 0) {
                    quiz_data.forEach(
                        (currentQuestion, questionNumber) => {
                            if (currentQuestion.id > questions) {
                                questions = currentQuestion.id;
                            }
                        }
                    );
                }
                questions++;
                let button;
                if (question_type === 'open') {
                    button = buttonAddAnswer(questions);
                } else if (question_type === 'test') {
                    button = buttonAddAnswer(questions);
                }
                document.getElementById('quiz-content').innerHTML = showQuestionContent(questions, tinymce.activeEditor.getContent(), '', button);
                quiz_data.push({
                    question: tinymce.activeEditor.getContent(),
                    answers: [],
                    correct_answer: '',
                    type: question_type,
                    id: questions
                });
                formQuestionsList();
                closeEditor();
                saveUpdates();
            }
        }
    } else {
        alert("Please finish editing and save before opening a new editor");
    }
};

function addAnswer(id) {
    if (!tinymce.activeEditor) {
        let data = id.split('-');
        let question_id;
        let type;
        question_id = data[0];
        jQuery.grep(quiz_data, function (question) {
            if (question.id == question_id) {
                type = question.type;
            }
        });
        document.getElementById('tinymce-answer-area-' + question_id).innerHTML += createTinyMCEArea();
        initiateMCE();
        document.getElementById("save-text").onclick = function () {
            let question_index = quiz_data.findIndex(question => question.id == question_id);
            let last_answer = getLastAnswer(question_index);
            if (!quiz_data[question_index].answers) {
                quiz_data[question_index].answers = [];
            }
            quiz_data[question_index].answers.push({id: last_answer, answer: tinymce.activeEditor.getContent()});
            if (type === 'open') {
                quiz_data[question_index].correct_answer = tinymce.activeEditor.getContent({format: "text"}).trim();
                document.getElementById('answers-list-' + question_id).innerHTML += openAnswerContent(question_id, last_answer, tinymce.activeEditor.getContent());
                document.getElementById(id).remove();
            } else if (type === 'test') {
                document.getElementById('answers-list-' + question_id).innerHTML += testAnswerContent(question_id, last_answer, tinymce.activeEditor.getContent());
            }
            closeEditor();
            saveUpdates();
        };
        document.getElementById("close-editor").onclick = function () {
            closeEditor();
        }
    } else {
        alert("Please finish editing and save before opening a new editor");
    }
}

function markCorrect(id) {
    let data = id.split('-');
    let question_index = quiz_data.findIndex(question => question.id == data[1]);
    if (quiz_data[question_index].correct_answer !== '') {
        document.getElementById('correct-' + data[1] + '-' + quiz_data[question_index].correct_answer).classList.add("btn-outline");
        quiz_data[question_index].correct_answer = data[2];
        document.getElementById(id).classList.remove("btn-outline");
        saveUpdates();
    } else {
        quiz_data[question_index].correct_answer = data[2];
        document.getElementById(id).classList.remove("btn-outline");
        saveUpdates();
    }
}

function editQuestionContent(id) {
    let data = id.split('-');
    if (!tinymce.activeEditor) {
        document.getElementById('tinymce-question-edit-area-' + data[2]).innerHTML += createTinyMCEArea();
        initiateMCE();
        document.getElementById('question-edit-' + data[2]).remove();
        document.getElementById('question-delete-' + data[2]).remove();
        tinymce.activeEditor.on('init', function (e) {
            tinymce.activeEditor.execCommand('mceInsertContent', false, document.getElementById('question-content-' + data[2]).innerHTML);
        });
        document.getElementById("save-text").onclick = function () {
            quiz_data = jQuery.grep(quiz_data, function (value) {
                if (value.id == data[2]) {
                    value.question = tinymce.activeEditor.getContent();
                }
                return value;
            });
            document.getElementById('question-content-' + data[2]).innerHTML = tinymce.activeEditor.getContent() + buttonEditQuestion(data[2]) + buttonDeleteQuestion(data[2]);
            closeEditor();
            formQuestionsList();
            saveUpdates();
        };
        document.getElementById("close-editor").onclick = function () {
            closeEditor();
            document.getElementById('question-content-' + data[2]).innerHTML += buttonEditQuestion(data[2]) + buttonDeleteQuestion(data[2]);
        }
    } else {
        alert("Please finish editing and save before opening a new editor");
    }
}

function editAnswerContent(id) {
    let data = id.split('-');
    let type;
    let content = '';
    if (!tinymce.activeEditor) {
        if (document.getElementById('correct-' + data[1] + '-' + data[2])) {
            type = 'test';
            document.getElementById('correct-' + data[1] + '-' + data[2]).remove();
        }
        if (type !== 'test') {
            content += buttonEditAnswer(data[1], data[2]) + buttonDeleteAnswer(data[1], data[2]);
        }
        openEditAnswerArea(data);
        document.getElementById("save-text").onclick = function () {
            quiz_data = jQuery.grep(quiz_data, function (question) {
                if (question.id == data[1]) {
                    question.answers = jQuery.grep(question.answers, function (answer) {
                        if (type === 'test' && answer.id == data[2]) {
                            answer.answer = tinymce.activeEditor.getContent();
                            if (answer.id == question.correct_answer) {
                                content += buttonMarkCorrect(data[1], data[2], '') + buttonEditAnswer(data[1], data[2]) + buttonDeleteAnswer(data[1], data[2]);
                            } else {
                                content += buttonMarkCorrect(data[1], data[2]) + buttonEditAnswer(data[1], data[2]) + buttonDeleteAnswer(data[1], data[2]);
                            }
                        }
                        return answer;
                    });
                    if (!type) {
                        question.correct_answer = tinymce.activeEditor.getContent({format: "text"}).trim();
                    }
                }
                return question;
            });
            document.getElementById('answer-' + data[1] + '-' + data[2]).innerHTML = tinymce.activeEditor.getContent() + content;
            closeEditor();
            saveUpdates();
        };
        document.getElementById("close-editor").onclick = function () {
            document.getElementById('answer-' + data[1] + '-' + data[2]).innerHTML += content;
            closeEditor();
        }
    } else {
        alert("Please finish editing and save before opening a new editor");
    }
}

function deleteQuestion(id) {
    if (confirm('Are you sure you want to delete question?')) {
        let data = id.split('-');
        quiz_data = jQuery.grep(quiz_data, function (value) {
            return value.id != data[2];
        });
        document.getElementById('question-' + data[2]).remove();
        formQuestionsList();
        saveUpdates();
    }
}

function deleteAnswer(id) {
    if (confirm('Are you sure you want to delete this answer?')) {
        let data = id.split('-');
        let type;
        quiz_data = jQuery.grep(quiz_data, function (question) {
            if (question.id == data[1]) {
                question.answers = jQuery.grep(question.answers, function (answer) {
                    return answer.id != data[2];
                });
                if (question.correct_answer == data[2]) {
                    question.correct_answer = '';
                }
                type = question.type;
            }
            return question;
        });
        if (type === 'open') {
            document.getElementById('tinymce-answer-area-' + data[1]).innerHTML += buttonAddAnswer(data[1]);
        }
        document.getElementById('answer-' + data[1] + '-' + data[2]).remove();
        saveUpdates();
    }
}

function closeEditor() {
    tinymce.remove();
    question_type = '';
    document.getElementById("close-editor").remove();
    document.getElementById("save-text").remove();
    document.getElementById("tinymce").remove();
    if (document.getElementById('open-answer')) {
        document.getElementById('open-answer').remove();
    }
    if (document.getElementById('test-answer')) {
        document.getElementById('test-answer').remove();
    }
}

function openEditAnswerArea(data) {
    document.getElementById('tinymce-answer-area-' + data[1]).innerHTML += createTinyMCEArea();
    initiateMCE();
    document.getElementById('edit-' + data[1] + '-' + data[2]).remove();
    document.getElementById('delete-' + data[1] + '-' + data[2]).remove();
    tinymce.activeEditor.on('init', function (e) {
        tinymce.activeEditor.execCommand('mceInsertContent', false, document.getElementById('answer-' + data[1] + '-' + data[2]).innerHTML);
    });
}

// Initiating interface

formQuestionsList();
displayQuestion();