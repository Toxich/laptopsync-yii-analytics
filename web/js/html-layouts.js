// MANAGE QUIZ FUNCTIONALITY

// BUTTONS

function buttonEditAnswer(question_id, answer_id) {
    return '<button class="btn manage-button btn-info btn-outline" id="edit-' + question_id + '-' + answer_id + '" onclick="editAnswerContent(this.id)">Edit</button>'
}

function buttonAddAnswer(question_id) {
    return '<button class="btn btn-primary add-answer" id="' + question_id + '" onclick="addAnswer(this.id)" > Add answer </button>';
}

function buttonDeleteAnswer(question_id, answer_id) {
    return '<button class="btn manage-button btn-danger" id="delete-' + question_id + '-' + answer_id + '" onclick="deleteAnswer(this.id)">Delete</button>';
}

function buttonMarkCorrect(question_id, answer_id, button_style = 'btn-outline' ) {
    return '<button class="btn manage-button btn-success ' + button_style +'" id="correct-' + question_id + '-' + answer_id + '" onclick="markCorrect(this.id)">Mark as correct</button>';
}

function buttonDeleteQuestion(question_id) {
    return '<button class="btn manage-button btn-danger" id="question-delete-' + question_id + '" onclick="deleteQuestion(this.id)">Delete</button>';
}

function buttonEditQuestion(question_id) {
    return '<button class="btn manage-button btn-info btn-outline" id="question-edit-' + question_id + '" onclick="editQuestionContent(this.id)">Edit</button>';
}

function buttonSaveText() {
    return ' <button class="btn btn-primary" id="save-text">Save</button>';
}

function buttonCloseEditor() {
    return '<button class="btn btn-danger" id="close-editor" onclick="closeEditor()">Close</button>';
}

function buttonSetTypeOpenAnswer(id) {
    return '<button class="btn btn-info btn-outline" id="open-answer" name="' + id + '" onclick="setAnswerType(this.id)">Open-ended answer</button>';
}

function buttonSetTypeTestAnswer(id) {
    return '<button class="btn btn-warning btn-outline" id="test-answer" name="' + id + '" onclick="setAnswerType(this.id)"> Multiple choice question</button>';
}

// TINYMCE

function createQuestionTinyMCEArea(questions) {
    return '<textarea id="tinymce" placeholder="Please enter your question" ></textarea>' +
            buttonSaveText() +
            buttonCloseEditor() +
            buttonSetTypeOpenAnswer(questions) +
            buttonSetTypeTestAnswer(questions);
}

function createTinyMCEArea() {
    return '<textarea id="tinymce" placeholder="Please enter your answer" ></textarea>' +
            buttonSaveText() +
            buttonCloseEditor()
}

// ANSWERS

function openAnswerContent(question_id, answer_id, answer) {
    return '<li class="list-group-item answers-list-item" id="answer-' + question_id + '-' + answer_id + '" >' +
                answer +
                buttonEditAnswer(question_id, answer_id) +
                buttonDeleteAnswer(question_id, answer_id) +
            '</li>';
}

function testAnswerContent(question_id, answer_id, answer, button_style) {
    return '<li class="list-group-item answers-list-item" id="answer-' + question_id + '-' + answer_id + '" >' +
                answer +
                buttonMarkCorrect(question_id, answer_id, button_style) +
                buttonEditAnswer(question_id, answer_id) +
                buttonDeleteAnswer(question_id, answer_id) +
            '</li>';
}

// QUESTIONS

function questionsListElement(id, question) {
    return '<li class="list-group-item answers-list-item" id="list-question-' + id + '" onclick="displayQuestion(this.id)">' +
                question +
            '</li>';
}

function showQuestionContent(question_id, question, answer_content = '', add_button_content = '') {
    return  '<div class="question-box panel panel-default" id="question-' + question_id + '" >' +
                '<div class="question-header panel-heading" id="question-content-' + question_id + '" >' +
                    question + buttonEditQuestion(question_id) +
                    buttonDeleteQuestion(question_id) +
                '</div>' +
            '<div class="tinymce-question-edit-area" id="tinymce-question-edit-area-' + question_id + '" ></div>' +
            '<ul class="list-group" id="answers-list-' + question_id + '">' +
                  answer_content +
            '</ul>' +
            add_button_content +
            '<div class="tinymce-answer-area" id="tinymce-answer-area-' + question_id + '" >' +
            '</div>' +
            '</div>';
}

// QUIZ FUNCTIONALITY

function openAnswerInput(id) {
    return `<label>
                <input type="text" class="open-answer" id="${id}" name="question-${id}">
            </label>`;
}

function radioAnswerInput(id, question, idIndex) {
    return `<label class="radio-answers"> 
                <input type="radio" id="${id}-${idIndex}" class="answer-input" name="question-${id}" value="${question.answers[letter].id}">
                ${question.answers[letter].id} : ${question.answers[letter].answer}
            </label>`;
}

function quizSlide(question, answers) {
    return `<div class="slide">
                   <div class="question"> ${question.question} </div>
                   <div class="answers"> ${answers.join("")} </div>
            </div>`;
}

function successfulAnswer() {
    return  '<div class="alert alert-success" role="alert">' +
                ' You are right, great!' +
            '</div>'
}

function wrongAnswer(userAnswer, correctAnswer) {
    return     '<div class="panel panel-danger">' +
                        '<div class="panel-heading">Your answer is wrong</div>' +
                        '<div class="panel-body">' +
                                '<p>Your answer: </p>' + userAnswer +
                        '</div>' +
                        '<div class="panel-footer" >' +
                                '<p>Correct answer: </p>' + correctAnswer +
                        '</div>' +
                        '<button class="btn btn-success" onclick="correctAnswerShowed()" >Ok!</button>' +
                '</div>'

}

function successfulParsing(message) {
    return  '<div class="alert alert-success" role="alert">' +
                message +
            '</div>';
}

function failedParsing(message) {
    return  '<div class="alert alert-danger" role="alert">' +
                message +
            '</div>';
}