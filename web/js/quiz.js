// Functions
function checkQuiz() {
    if (myQuestions.length === 0) {
        alert("Quiz is empty");
    } else {
        let validated_questions = 0;
        myQuestions.forEach(function (question) {
            if (question.answers) {
                if (question.answers.length !== 0 && question.correct_answer !== '') {
                    validated_questions++;
                }
            }
        });
        if (validated_questions !== myQuestions.length) {
            validated_questions = 0;
            alert("Quiz is not finished yet, you can try to pass it but some errors are possible!");
        }
    }
}

function buildQuiz() {
    // variable to store the HTML output
    const output = [];
    // for each question...
    let idIndex = 0;
    myQuestions.forEach(
        (currentQuestion, questionNumber) => {

            // variable to store the list of possible answers
            const answers = [];

            // and for each available answer inputs
            for (letter in currentQuestion.answers) {
                if (currentQuestion.type === 'open') {
                    answers.push(openAnswerInput(questionNumber));
                } else {
                    idIndex++;
                    answers.push(radioAnswerInput(questionNumber, currentQuestion, idIndex));
                }
            }
            if ((!currentQuestion.answers || currentQuestion.answers < 1) && currentQuestion.type === 'open') {
                currentQuestion.answers = [];
                answers.push(openAnswerInput(questionNumber));
            }

            // add this question and its answers to the output
            output.push(quizSlide(currentQuestion, answers));
            idIndex = 0;
        }
    );

    // finally combine our output list into one string of HTML and put it on the page
    quizContainer.innerHTML = output.join('');
}

function showResults() {

    // gather answer containers from our quiz
    const answerContainers = quizContainer.querySelectorAll('.answers');

    // keep track of user's answers
    let numCorrect = 0;
    let userAnswersList = [];
    // for each question...
    myQuestions.forEach((currentQuestion, questionNumber) => {
        // find selected answer
        let userAnswer;
        if (currentQuestion.type && currentQuestion.type === 'open') {
            const answerContainer = answerContainers[questionNumber];
            const selector = `input[name=question-${questionNumber}]`;
            userAnswer = (answerContainer.querySelector(selector) || {}).value;
        } else {
            const answerContainer = answerContainers[questionNumber];
            const selector = `input[name=question-${questionNumber}]:checked`;
            userAnswer = (answerContainer.querySelector(selector) || {}).value;
        }
        userAnswersList.push({question_id: currentQuestion.id, answer: userAnswer, type: currentQuestion.type});
        // if answer is correct
        if (userAnswer === currentQuestion.correct_answer) {
            // add to the number of correct answers
            numCorrect++;
        }
    });
    $.ajax({
        url: "/quiz/save-results",
        type: "POST",
        dataType: 'json',
        data: {
            user_answers: userAnswersList,
            correct: numCorrect,
            quiz_id: quiz_id
        },
        success: function (data) {
            if (data) {
                window.location = '/quiz/results'
            } else {
                window.location = '/'
            }
        },
        error: function (data) {
            console.log(data);
            alert("ERROR");
        }
    });
}

function notEmptyTextInput() {
    let inputs = $("input[type=text]");
    let index;
    for (index = 0; index < inputs.length; ++index) {
        let input = inputs[index];
        input.value = '';
    }
}

function notEmptyRadioInput() {
    let inputs = $("input[type=radio]");
    let index;
    for (index = 0; index < inputs.length; ++index) {
        let input = inputs[index];
        if (input.checked) {
            input.checked = false;
            input.value = '';
        }
    }
}

function showSlide(n) {
    slides[currentSlide].classList.remove('active-slide');
    slides[n].classList.add('active-slide');
    currentSlide = n;
    slides[n].disabled = false;
    if (currentSlide === slides.length - 1) {
        nextButton.style.display = 'none';
        let answered;
        jQuery.grep(answeredQuestions, function (question) {
            if (question.id == n) {
                answered = true;
            }
        });
        submitButton.style.display = 'inline-block';
        submitButton.disabled = answered !== true;
    } else {
        nextButton.style.display = 'inline-block';
        let answered;
        jQuery.grep(answeredQuestions, function (question) {
            if (question.id == n) {
                answered = true;
            }
        });
        nextButton.disabled = answered !== true;
        submitButton.style.display = 'none';
    }
}

function answerQuestion() {
    if (myQuestions[currentSlide].type === 'test') {
        let index = 0;
        if (myQuestions[currentSlide].answers && myQuestions[currentSlide].answers.length > 0) {
            myQuestions[currentSlide].answers.forEach(function (answer) {
                index++;
                document.getElementById(currentSlide + "-" + index).disabled = true;
            })
        } else {
            alert("Error occured, probably it is because quiz is not finished.")
            return false;
        }
    } else {
        document.getElementById(currentSlide).disabled = true;
    }
    if (myQuestions[currentSlide].correct_answer === answeredQuestions[currentSlide].answer) {
        document.getElementById('answer-result').innerHTML = successfulAnswer();
        window.setTimeout(function() {
            $(".alert").slideUp(500, function() {
                $(this).remove();
            });
            showNextSlide();
        }, 2500);
    } else {
        if (myQuestions[currentSlide].type === 'test') {
            let correctAnswer = getAnswer(myQuestions[currentSlide].answers, myQuestions[currentSlide].correct_answer);
            let userAnswer = getAnswer(myQuestions[currentSlide].answers, answeredQuestions[currentSlide].answer);
            if (correctAnswer && correctAnswer.length > 0) {
                document.getElementById('answer-result').innerHTML = wrongAnswer(userAnswer[0].answer, correctAnswer[0].answer);
            } else {
                alert("Creator didn't set up correct answer in this question");
                showNextSlide();
            }
        } else {
            if (myQuestions[currentSlide].correct_answer) {
                document.getElementById('answer-result').innerHTML = wrongAnswer(answeredQuestions[currentSlide].answer, myQuestions[currentSlide].correct_answer);
            } else {
                alert("Creator didn't set up correct answer in this question");
                showNextSlide();
            }
        }
    }
}

function correctAnswerShowed() {
    $(".panel").slideUp(500, function () {
        $(this).remove();
    });
    showNextSlide();
}

function getAnswer(answers, id) {
    return jQuery.grep(answers, function (answer) {
        if (answer.id == id) {
            return answer;
        }
    });
}

function showNextSlide() {
    showSlide(currentSlide + 1);
}


function fillProgress(event, input) {
    let check_answered;
    let data = event.target.id.split('-');
    let question_id = data[0];
    answeredQuestions.forEach((currentQuestion, questionNumber) => {
        if (currentQuestion.id == question_id) {
            check_answered = true;
        }
    });
    if (input.val() && input.val() !== '') {
        let answer = input.val();
        if (check_answered !== true) {
            answeredQuestions.push({id: question_id, answer: input.val()});
            let percent = (answeredQuestions.length / quiz_length) * 100;
            document.getElementById('progress').style.width = percent + '%';
            nextButton.disabled = false;
            if (currentSlide === slides.length - 1) {
                submitButton.disabled = false;
            }
        } else {
            answeredQuestions = jQuery.grep(answeredQuestions, function (question) {
                if (question.id == question_id) {
                    question.answer = answer;
                }
                return question;
            });
        }
    } else if (input.val() === '' && check_answered === true) {
        answeredQuestions = jQuery.grep(answeredQuestions, function (question) {
            if (question.id !== question_id) {
                return question;
            }
        });
        let percent = (answeredQuestions.length / quiz_length) * 100;
        document.getElementById('progress').style.width = percent + '%';
        nextButton.disabled = true;
        if (currentSlide === slides.length - 1) {
            submitButton.disabled = true;
        }
    }
}

// Variables
const quizContainer = document.getElementById('quiz');
const resultsContainer = document.getElementById('results');
const submitButton = document.getElementById('submit');
let quiz_length = myQuestions.length;
let answeredQuestions = [];

//Initiating
buildQuiz();
checkQuiz();

// Pagination
const nextButton = document.getElementById("next");
const slides = document.querySelectorAll(".slide");
let currentSlide = 0;

// Show the first slide
showSlide(currentSlide);

// Event listeners
submitButton.addEventListener('click', showResults);
nextButton.addEventListener("click", answerQuestion);

$(window).bind("pageshow", function () {
    notEmptyTextInput();
    notEmptyRadioInput();
});
$("input[type=radio]").on('input', function (event) {
    fillProgress(event, $(this));
});
$("input[type=text]").on('input', function (event) {
    fillProgress(event, $(this));
});