function parseByUrl() {
    let url = document.getElementById('parse-url-input').value;
    if (url !== '') {
        $.ajax({
            url: "/feeds/get-by-url",
            type: "POST",
            dataType: 'json',
            data: {
                url: url,
            },
            success: function (data) {
                if (data.result) {
                    document.getElementById('results').innerHTML += successfulParsing(data.message);
                } else {
                    document.getElementById('results').innerHTML += failedParsing(data.message);
                }
                window.setTimeout(function() {
                    $(".alert").slideUp(500, function() {
                        $(this).remove();
                    });
                }, 2500);
            },
            error: function (data) {
                console.log(data);
                alert("ERROR");
            }
        });
    } else {
        alert('Please enter url first!')
    }
}