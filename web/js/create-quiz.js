document.getElementById("create-quiz-button").onclick = function () {
    if (document.getElementById("quiz-name").value === '') {
        alert('Please enter quiz name');
    } else {
        $.ajax({
            url: "/quiz/save",
            type: "POST",
            dataType: 'json',
            data: {
                name: document.getElementById("quiz-name").value,
            },
            success: function (data) {
                window.location = '/quiz/update?id=' + data;
            },
            error: function (data) {
                console.log(data);
                alert("ERROR");
            }
        });
    }
};