# Analytics

## Installation
Run the folowing commands: 

1. Swith/checkout to master branch

```bash
git checkout master
```


2. Remove next folder:

```bash
rm -rf ./vendor/
rm -rf ./composer.lock
```


3. Run the following commands for install composer:

```bash 
composer global require "fxp/composer-asset-plugin"
composer install
```

5. Add these params to web.php

```bash
'params' => $params,
    'aliases' => [
        '@bower' => '@vendor/bower',
        '@npm' => '@vendor/npm-asset',
    ],
```

6. Create database 'analytics' and make db with this file: 
```bash 
analytics-table-new.sql
```

7. Run migration
```bash 
php yii migrate
```