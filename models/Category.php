<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property integer $id
 * @property integer $member_post_id
 * @property string $category_title
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_post_id' => 'Member Post ID',
            'category_title' => 'Category Title'
        ];
    }

    public static function create($categoryTitle, $postId)
    {
        $postCategory = new Category();
        $postCategory->member_post_id = $postId;
        $postCategory->category_title = $categoryTitle;
        $postCategory->save();
    }

}
