<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property integer $id
 * @property string $name
 * @property json $quiz_data
 * @property timestamp $created_at
 */
class Quiz extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quiz';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Quiz name',
            'quiz_Data' => 'Quiz data',
            'created_at' => 'Created at',
        ];
    }

    public static function getById($id, $array)
    {
        foreach ($array as $element) {
            if ($element['id'] == $id) {
                return $element;
            }
        }
    }

}
