<?php

namespace app\models;

use app\models\PurchaseConversionsArchive;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property integer $active
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'active'], 'required'],
            [['user_id', 'active', 'to_delete'], 'integer'],
            [['title', 'domain', 'screenshot'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'active' => 'Active',
        ];
    }
	
	
	

	 
	 

    
	
 
			 
 
	 
	 
	 
	 
	 
 
}
