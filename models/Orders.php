<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $admin_user_id
 * @property string $charge_id
 * @property integer $people_id
 * @property string $product_ids
 * @property string $variant_ids
 * @property string $product_json
 * @property string $coupon_used
 * @property string $name
 * @property string $tracking_number
 * @property integer $shipping_id
 * @property integer $customer_id
 * @property string $shipping_name
 * @property string $shipping_address
 * @property string $shipping_address_line2
 * @property string $shipping_city
 * @property string $shipping_province_state
 * @property integer $shipping_zipcode
 * @property string $shipping_country
 * @property string $phone_number
 * @property integer $funnel_id
 * @property integer $status
 * @property string $date
 * @property string $subtotal
 * @property string $tax
 * @property string $total
 * @property integer $fulfilled
 * @property integer $shipping_offer_id
 * @property integer $discount_offer_id
 * @property string $subtraction_amount
 * @property integer $payments_taken
 * @property integer $payments_total
 * @property integer $payment_type
 * @property integer $refunded
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'admin_user_id', 'people_id', 'shipping_id', 'customer_id', 'shipping_zipcode', 'funnel_id', 'status', 'fulfilled', 'shipping_offer_id', 'discount_offer_id', 'payments_taken', 'payments_total', 'payment_type', 'refunded'], 'integer'],
            [['charge_id', 'people_id', 'coupon_used', 'shipping_offer_id', 'discount_offer_id', 'subtraction_amount'], 'required'],
            [['product_json'], 'string'],
            [['subtotal', 'tax', 'total', 'subtraction_amount'], 'number'],
            [['charge_id', 'product_ids', 'variant_ids', 'coupon_used', 'name', 'tracking_number', 'shipping_name', 'shipping_address', 'shipping_address_line2', 'shipping_city', 'shipping_province_state', 'shipping_country', 'phone_number', 'date'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'admin_user_id' => 'Admin User ID',
            'charge_id' => 'Charge ID',
            'people_id' => 'People ID',
            'product_ids' => 'Product Ids',
            'variant_ids' => 'Variant Ids',
            'product_json' => 'Product Json',
            'coupon_used' => 'Coupon Used',
            'name' => 'Name',
            'tracking_number' => 'Tracking Number',
            'shipping_id' => 'Shipping ID',
            'customer_id' => 'Customer ID',
            'shipping_name' => 'Shipping Name',
            'shipping_address' => 'Shipping Address',
            'shipping_address_line2' => 'Shipping Address Line2',
            'shipping_city' => 'Shipping City',
            'shipping_province_state' => 'Shipping Province State',
            'shipping_zipcode' => 'Shipping Zipcode',
            'shipping_country' => 'Shipping Country',
            'phone_number' => 'Phone Number',
            'funnel_id' => 'Funnel ID',
            'status' => 'Status',
            'date' => 'Date',
            'subtotal' => 'Subtotal',
            'tax' => 'Tax',
            'total' => 'Total',
            'fulfilled' => 'Fulfilled',
            'shipping_offer_id' => 'Shipping Offer ID',
            'discount_offer_id' => 'Discount Offer ID',
            'subtraction_amount' => 'Subtraction Amount',
            'payments_taken' => 'Payments Taken',
            'payments_total' => 'Payments Total',
            'payment_type' => 'Payment Type',
            'refunded' => 'Refunded',
        ];
    }
    public static function getMinDateByProjectID($project_id)
    {
        try {
            $date = Orders::find(['project_id' => $project_id])->orderBy(['date' => SORT_ASC])->one()['date'];
        } catch (NotFoundHttpException $exception) {
            return null;
        }
        return $date;

    }

    public static function getMaxDateProjectID($project_id)
    {
        try {
            $date = Orders::find(['project_id' => $project_id])->orderBy(['date' => SORT_DESC])->one()['date'];
        } catch (NotFoundHttpException $exception) {
            return null;
        }
        return $date;

    }

    public static function getDataByDatesRevenueAndOrders($project, $from, $to)
    {
        try {
            $query = (new \yii\db\Query())
            ->select(["DATE_FORMAT(FROM_UNIXTIME(date), '%Y-%m-%d') AS ndate", "SUM(total) AS revenue", "COUNT(total) AS orders"])
            ->from('orders')
            ->where(['project_id' => $project])
            ->andWhere(["<=", "DATE_FORMAT(FROM_UNIXTIME(date), '%Y-%m-%d %H:%i:%s')", $to])
            ->andWhere([">=", "DATE_FORMAT(FROM_UNIXTIME(date), '%Y-%m-%d/ %H:%i:%s')", $from])
            ->groupBy('ndate')
            ->all();
        } catch (NotFoundHttpException $exception) {
            return null;
        }
        return $query;
    }

    public static function getSumArrayByKey($array, $key)
    {
        $dataSum = array_sum(array_map(function($item) use($key) { 
            return $item["{$key}"]; 
        }, $array));

        return $dataSum;
    }
}
