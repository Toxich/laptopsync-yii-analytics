<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "people".
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $project_id
 * @property string $email
 * @property integer $email_validated
 * @property string $password
 * @property string $recover_code
 * @property string $firstname
 * @property string $lastname
 * @property string $shipping_address
 * @property string $shipping_address_line2
 * @property string $shipping_city
 * @property string $shipping_province_state
 * @property string $shipping_country
 * @property string $shipping_zipcode
 * @property string $billing_address
 * @property string $billing_address_line2
 * @property string $billing_city
 * @property string $billing_province_state
 * @property string $billing_country
 * @property string $billing_zipcode
 * @property string $ip_address
 * @property integer $visits
 * @property string $date_acquired
 * @property string $tags
 * @property integer $unique_user_id
 * @property integer $first_optin_id
 * @property integer $is_customer
 * @property integer $ltv
 * @property string $product_ids
 * @property integer $num_orders
 * @property integer $payment_gateway_customer_id
 * @property string $stripe_token
 * @property string $stripe_customer_id
 * @property integer $yearmonthdate
 * @property string $hash
 * @property integer $confirmed
 * @property integer $unsubscribed
 */
class People extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'project_id', 'email_validated', 'visits', 'unique_user_id', 'first_optin_id', 'is_customer', 'ltv', 'num_orders', 'payment_gateway_customer_id', 'yearmonthdate', 'confirmed', 'unsubscribed'], 'integer'],
            [['project_id', 'email', 'date_acquired', 'confirmed', 'unsubscribed'], 'required'],
            [['email', 'password', 'recover_code', 'firstname', 'lastname', 'shipping_address', 'shipping_address_line2', 'shipping_city', 'shipping_province_state', 'shipping_country', 'shipping_zipcode', 'billing_address', 'billing_address_line2', 'billing_city', 'billing_province_state', 'billing_country', 'billing_zipcode', 'ip_address', 'date_acquired', 'tags', 'product_ids', 'stripe_token', 'stripe_customer_id', 'hash'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_id' => 'Account ID',
            'project_id' => 'Project ID',
            'email' => 'Email',
            'email_validated' => 'Email Validated',
            'password' => 'Password',
            'recover_code' => 'Recover Code',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'shipping_address' => 'Shipping Address',
            'shipping_address_line2' => 'Shipping Address Line2',
            'shipping_city' => 'Shipping City',
            'shipping_province_state' => 'Shipping Province State',
            'shipping_country' => 'Shipping Country',
            'shipping_zipcode' => 'Shipping Zipcode',
            'billing_address' => 'Billing Address',
            'billing_address_line2' => 'Billing Address Line2',
            'billing_city' => 'Billing City',
            'billing_province_state' => 'Billing Province State',
            'billing_country' => 'Billing Country',
            'billing_zipcode' => 'Billing Zipcode',
            'ip_address' => 'Ip Address',
            'visits' => 'Visits',
            'date_acquired' => 'Date Acquired',
            'tags' => 'Tags',
            'unique_user_id' => 'Unique User ID',
            'first_optin_id' => 'First Optin ID',
            'is_customer' => 'Is Customer',
            'ltv' => 'Ltv',
            'product_ids' => 'Product Ids',
            'num_orders' => 'Num Orders',
            'payment_gateway_customer_id' => 'Payment Gateway Customer ID',
            'stripe_token' => 'Stripe Token',
            'stripe_customer_id' => 'Stripe Customer ID',
            'yearmonthdate' => 'Yearmonthdate',
            'hash' => 'Hash',
            'confirmed' => 'Confirmed',
            'unsubscribed' => 'Unsubscribed',
        ];
    }

    public static function getDataByDatesRevenueAndOrders($project, $from, $to)
    {
        try {
            $query = (new \yii\db\Query())
            ->select(["DATE_FORMAT(FROM_UNIXTIME(date_acquired), '%Y-%m-%d') AS ndate", "COUNT(date_acquired) AS people"])
            ->from('people')
            ->where(['project_id' => $project])
            ->andWhere([">=", "DATE_FORMAT(FROM_UNIXTIME(date_acquired), '%Y-%m-%d %H:%i:%s')", $from])
            ->andWhere(["<=", "DATE_FORMAT(FROM_UNIXTIME(date_acquired), '%Y-%m-%d %H:%i:%s')", $to])
            ->groupBy('ndate')
            ->all();
        } catch (NotFoundHttpException $exception) {
            return null;
        }
        return $query;
    }

    public static function getSumArrayByKey($array, $key)
    {
        $dataSum = array_sum(array_map(function($item) use($key) { 
            return $item["{$key}"]; 
        }, $array));

        return $dataSum;
    }
}
