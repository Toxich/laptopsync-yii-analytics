<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property integer $id
 * @property string $canonical_url
 * @property integer $member_post_id
 */
class PostUrl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'canonical_urls';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'canonical_url' => 'Canonical Url',
            'member_post_id' => 'Member Post ID'
        ];
    }

    public static function create($url, $postId)
    {
        $postUrl = new PostUrl();
        $postUrl->canonical_url = $url;
        $postUrl->member_post_id = $postId;
        $postUrl->save();
    }

}
