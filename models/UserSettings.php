<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_settings".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $brand_name
 * @property string $url
 * @property string $brand_logo
 * @property string $facebook
 * @property integer $gdpr_off
 * @property integer $agency_id
 * @property integer $agency_client_id
 * @property integer $agency_permissions
 * @property string $account_type
 * @property string $gdpr_terms
 * @property string $support_email
 * @property string $industry
 * @property string $employees
 * @property string $email_list
 * @property string $tool
 * @property string $smtp_host
 * @property string $smtp_username
 * @property string $smtp_password
 * @property string $smtp_from_email
 * @property integer $smtp_tested
 * @property integer $add_terms_checkbox
 * @property string $marketing_email_checkbox_wording
 * @property string $find_out
 * @property integer $tracked_sales_month
 * @property integer $tracked_participants_month
 * @property integer $extra_participants_month
 * @property integer $extra_tracked_sales_month
 * @property integer $permanent_extra_participants
 * @property integer $permanent_extra_sales
 * @property integer $grandfathered
 * @property string $instagram
 * @property string $twitter
 * @property string $pinterest
 * @property string $facebook_group
 * @property string $large_brand_image
 * @property string $logo_emails
 * @property string $youtube
 * @property string $reward_image
 * @property string $reward
 * @property integer $warning
 * @property string $cancellation_reason
 */
class UserSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'brand_name', 'url', 'brand_logo', 'facebook', 'smtp_tested', 'add_terms_checkbox', 'marketing_email_checkbox_wording', 'find_out', 'tracked_sales_month', 'tracked_participants_month', 'extra_participants_month', 'extra_tracked_sales_month', 'permanent_extra_participants', 'permanent_extra_sales', 'grandfathered', 'instagram', 'twitter', 'pinterest', 'facebook_group', 'large_brand_image', 'logo_emails', 'youtube', 'reward_image', 'reward', 'warning', 'cancellation_reason'], 'required'],
            [['user_id', 'gdpr_off', 'agency_id', 'agency_client_id', 'agency_permissions', 'smtp_tested', 'add_terms_checkbox', 'tracked_sales_month', 'tracked_participants_month', 'extra_participants_month', 'extra_tracked_sales_month', 'permanent_extra_participants', 'permanent_extra_sales', 'grandfathered', 'warning'], 'integer'],
            [['gdpr_terms', 'find_out', 'reward', 'cancellation_reason'], 'string'],
            [['brand_name', 'url', 'facebook', 'industry', 'employees', 'email_list', 'tool', 'smtp_host', 'smtp_username', 'smtp_password', 'smtp_from_email', 'marketing_email_checkbox_wording', 'instagram', 'twitter', 'pinterest', 'facebook_group', 'large_brand_image', 'logo_emails', 'youtube', 'reward_image'], 'string', 'max' => 255],
            [['brand_logo'], 'string', 'max' => 100],
            [['account_type'], 'string', 'max' => 3],
            [['support_email'], 'string', 'max' => 155],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'brand_name' => 'Brand Name',
            'url' => 'Url',
            'brand_logo' => 'Brand Logo',
            'facebook' => 'Facebook',
            'gdpr_off' => 'Gdpr Off',
            'agency_id' => 'Agency ID',
            'agency_client_id' => 'Agency Client ID',
            'agency_permissions' => 'Agency Permissions',
            'account_type' => 'Account Type',
            'gdpr_terms' => 'Gdpr Terms',
            'support_email' => 'Support Email',
            'industry' => 'Industry',
            'employees' => 'Employees',
            'email_list' => 'Email List',
            'tool' => 'Tool',
            'smtp_host' => 'Smtp Host',
            'smtp_username' => 'Smtp Username',
            'smtp_password' => 'Smtp Password',
            'smtp_from_email' => 'Smtp From Email',
            'smtp_tested' => 'Smtp Tested',
            'add_terms_checkbox' => 'Add Terms Checkbox',
            'marketing_email_checkbox_wording' => 'Marketing Email Checkbox Wording',
            'find_out' => 'Find Out',
            'tracked_sales_month' => 'Tracked Sales Month',
            'tracked_participants_month' => 'Tracked Participants Month',
            'extra_participants_month' => 'Extra Participants Month',
            'extra_tracked_sales_month' => 'Extra Tracked Sales Month',
            'permanent_extra_participants' => 'Permanent Extra Participants',
            'permanent_extra_sales' => 'Permanent Extra Sales',
            'grandfathered' => 'Grandfathered',
            'instagram' => 'Instagram',
            'twitter' => 'Twitter',
            'pinterest' => 'Pinterest',
            'facebook_group' => 'Facebook Group',
            'large_brand_image' => 'Large Brand Image',
            'logo_emails' => 'Logo Emails',
            'youtube' => 'Youtube',
            'reward_image' => 'Reward Image',
            'reward' => 'Reward',
            'warning' => 'Warning',
            'cancellation_reason' => 'Cancellation Reason',
        ];
    }
}
