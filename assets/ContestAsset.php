<?php
 
namespace app\assets;
 
use yii\web\AssetBundle;
 
class ContestAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
		'front_end/front_end_design_files/vyper-custom.css',
    ];
    public $js = [
		'front_end/front_end_design_files/pace.js',
    ];
    public $depends = [
    ];
}