<?php
 
namespace app\assets;
 
use yii\web\AssetBundle;
 
class BackendAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
		'backend/assets/css/style.css',
		'backend/assets/css/theme.css',
		'backend/assets/css/ui.css',
		'backend/assets/css/avenir-style.css',
		'backend/assets/plugins/datatables/dataTables.min.css'
    ];
    public $js = [

		'backend/assets/plugins/jquery/jquery-migrate-1.2.1.min.js',
		'backend/assets/plugins/jquery-ui/jquery-ui-1.11.2.min.js',
		'backend/assets/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js', 
		'backend/assets/plugins/bootstrap/js/bootstrap.min.js',
		'backend/assets/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js',
		'backend/assets/js/application.js',
		'backend/assets/js/plugins.js',
    ];
    public $depends = [
    ];
}