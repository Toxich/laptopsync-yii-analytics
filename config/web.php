<?php

$params = require(__DIR__ . '/params.php');


$hostname = $_SERVER['SERVER_NAME'];

switch ( strtolower($hostname))
{
case 'localhost':
case '127.0.0.1':
        $db_config = '/db.php';
        break;
default:
        $db_config = '/db_production.php';
        break;
}



$config = [
    'id' => 'basic',
	'name' => 'Analytics',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
	'modules' => [
	'charset' => 'utf8mb4',
	'redactor' => 'yii\redactor\RedactorModule',
	
	'gridview' =>  [
	        'class' => '\kartik\grid\Module'
	        // enter optional module parameters below - only if you need to  
	        // use your own export download action or custom translation 
	        // message source
	        // 'downloadAction' => 'gridview/export/download',
	        // 'i18n' => []
	    ],
		
	'user' => [
	'class' => 'dektrium\user\Module',
	'admins' => ['kevintang'],
	'controllerMap' => [
		'registration' => 'app\controllers\users\RegistrationController',
		'security' => 'app\controllers\users\SecurityController'
	],
	'modelMap' => [
		'User' => 'app\models\users\User',
		'LoginForm' => 'app\models\users\LoginForm'
	]
	/*
	'controllerMap' => [
	                'registration' => [
	                    'class' => dektrium\user\controllers\RegistrationController::className(),
	                    'on ' . dektrium\user\controllers\RegistrationController::EVENT_AFTER_REGISTER => function ($e) {
	                        $user = dektrium\user\models\User::findOne(['username'=>$e->form->username, 'email'=>$e->form->email]);

	                        if ($user) {
	                            Yii::$app->user->switchIdentity($user);
	                        }
	                        Yii::$app->response->redirect(Yii::$app->user->returnUrl);
	                    },
	                ],
	
	*/
	],
	],
    'components' => [


			
		
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'c5gz6Z7FjVdxOfF6UI6BcNBzmtWWGpWW',
        ],

		 'urlManager' => [
		          'showScriptName' => false,
		          'enablePrettyUrl' => true,
		'showScriptName' => false,
		            'rules' => [
		
					'gii' => 'gii',
					'gii/<controller:\w+>' => 'gii/<controller>',
					'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',
					'projects/<project_id:\d+>/overview/' => 'projects/overview'
		
					
					
					//'http://<brandname:\w+>.hyax.com/<id:\d+>' => 'contests/view',
							
							 // ...
						            ],
		
		],
		
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '/tmp',
        ],

      
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
   			'mail' => [
				
			    'class' => 'yii\swiftmailer\Mailer',
			               // send all mails to a file by default. You have to set
			               // 'useFileTransport' to false and configure a transport
			               // for the mailer to send real emails.
			               'useFileTransport' => true,
			    ],
		
					
			
		
			
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
       'db' => require(__DIR__ . $db_config),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
