<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Orders;
use app\models\Projects;
use app\models\People;
use yii\helpers\Url;
use kartik\date\DatePicker;
use scotthuangzl\googlechart\GoogleChart;



/* @var $this yii\web\View */
/* @var $model app\models\Projects */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(
    '@web/js/update-chart.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$admin_user_id = Yii::$app->user->getId();
$start_date = Yii::$app->request->get('start_date');
$form_title = "No Form Selected";

$date = date('m-d-Y');


?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

				<div class="page-title" style="position: relative; top: 0px;">
					 
				<h1 class="page-title-h1">Analytics</h1>
				
				
				</div>
    
	
	
	
	<div class="row" style="padding: 16px; padding-top: 0px;">
		
		
		
		
      
                <div class="panel panel-darkblue" style="border-top: 3px solid #02bc87">
	
		<div style="text-align: right">
			
			<div style="display: inline-block; text-align: left; width: 60%; padding-bottom: 30px; vertical-align: middle">
				Orders & Revenue Over Time
				</div>
					
		<div style="display: inline-block; vertical-align: middle; margin-right: 8px; padding-bottom: 20px;">	
					 
		  </div>
		  <div style="display: inline-block; vertical-align: middle; padding-bottom: 20px;">
		  
			<div class="datapicker form-inline">
				<input type="date" id="start"  class="form-control" name="trip-start" value="<?= $minDate ?>" min="<?= $minDate ?>" max="<?= $maxDate ?>" onkeydown="return false">
				<input type="date" id="end" class="form-control" name="trip-start" value="<?= $maxDate ?>" min="<?= $minDate ?>" max="<?= $maxDate ?>" onkeydown="return false">
				<input type="hidden" id="project" value="<?= $model->id ?>">
			</div>
			<button class='button is-primary' id="pickDate" style='min-height: 46px; margin: 5px'>Pick Date</button>
		  
		  </div>
		  
		  
		  
		  
		  
	  </div>
		  
					  <style>
					.datapicker {
						margin: 5px;
					}
					.krajee-datepicker {
						height: 40px;
					}	 
		
					.kv-date-calendar {
						width: 40px;
					}
						 </style>
						 
						
<div id="chart-container" style="min-width: 310px; height: 500px; margin: 0 auto; color: #000">
	
	<div id="chart" style="height: 400px"></div>
	<!-- CHART WILL SHOW HERE
	<br /><br />
	
	
	X AXIS (horizontal) is date <br />
	
	Y AXIS is revenue -->
</div>


			  
			  
			  
		  </div>

	</div>

	
	
		
		
		
		
		<div class="row">
		                            <div class="col-md-4">
		                                <div class="panel panel-darkblue">
		                                    <div class="panel-heading clearfix">
		                                        <h3 class="panel-title" style='font-size: 25px; text-align: center'>Revenue 
												<br />
												<span style='font-size: 14px; opacity: 0.7'><?= $date ?> (today)</span>
												
												</h3>
												
												
		                                    </div> <!-- end panel-heading -->
										 
											<div class="panel-body dashboard-panel" style="text-align: center">
											
												
												
												<span style='font-size: 40px;'>
													
											
													
													
													$ <?= $revenueOrdersToday ? doubleval($revenueOrdersToday['0']['revenue']) : 0?>
													
													 <img src="https://s3.us-east-2.amazonaws.com/001fvy/12bcb02832a0debe8d6cc0fbc4c5a19f9b98da96.png" style="width: 15px;">
												</span>
												
												<br /><br />
												
												<table class="table">
													<tr>
														<td>
														Past 7 Days
														</td>
														
														<td>
														$ <?= $revenue7PastDays ?  $revenue7PastDays : 0?>
														</td>
													</tr>
													<tr>
														<td>
														Past 14 Days
														</td>
														
														<td>
														$ <?= $revenue14PastDays ?  $revenue14PastDays : 0?>
														</td>
													</tr>
													<tr>
														<td>
														Past 30 Days
														</td>
														
														<td>
														$ <?= $revenue30PastDays ?  $revenue30PastDays : 0?>
														</td>
													</tr>
												
												</table>
												
												
											</div>
										
										
										
										</div> <!-- end panel-darkblue-->
									</div> <!-- end col-md-4-->
									
		                            <div class="col-md-4">
		                                <div class="panel panel-darkblue">
		                                    <div class="panel-heading clearfix">
		                                        <h3 class="panel-title" style='font-size: 25px; text-align: center'>Orders
												<br />
												<span style='font-size: 14px; opacity: 0.7'><?= $date ?> (today)</span>
												
												</h3>
		                                    </div> <!-- end panel-heading -->
										 
											<div class="panel-body dashboard-panel" style="text-align: center">
											
												
												
												<span style='font-size: 40px;'>
													<?= $revenueOrdersToday ? $revenueOrdersToday['0']['orders'] : 0 ?>
													<img src="https://s3.us-east-2.amazonaws.com/001fvy/12bcb02832a0debe8d6cc0fbc4c5a19f9b98da96.png" style="width: 15px;">
												</span>
												
												
												<br /><br />
												
												<table class="table">
													<tr>
														<td>
														Past 7 days
														</td>
														
														<td>
														<?=  $orders7PastDays ? $orders7PastDays : 0?>
														</td>
													</tr>
													<tr>
														<td>
														Past 14 Days
														</td>
														
														<td>
														<?=  $orders14PastDays ? $orders14PastDays : 0?>
														</td>
													</tr>
													<tr>
														<td>
												Past 30 Days
														</td>
														
														<td>
														<?=  $orders30PastDays ? $orders30PastDays : 0?>
														</td>
													</tr>
												</table>
												
												
												
												
											</div>
										
										
										
										</div> <!-- end panel-darkblue-->
									</div> <!-- end col-md-4-->
									
									
									
		                            <div class="col-md-4">
		                                <div class="panel panel-darkblue">
		                                    <div class="panel-heading clearfix">
		                                        <h3 class="panel-title" style='font-size: 25px; text-align: center'>New People
												<br />
												<span style='font-size: 14px; opacity: 0.7'><?= $date ?> (today)</span>
												
												</h3>
		                                    </div> <!-- end panel-heading -->
										 
											<div class="panel-body dashboard-panel" style="text-align: center">
											
												
												
												<span style='font-size: 40px;'>
													<?=  $peopleToday ? $peopleToday['0']['people'] : 0?>
												</span>
												
												
												<br /><br />
												
												<table class="table">
													<tr>
														<td>
														Past 7 days
														</td>
														
														<td>
														<?=  $people7PastDays ? $people7PastDays : 0?>
														</td>
													</tr>
													<tr>
														<td>
														Past 14 Days
														</td>
														
														<td>
														<?=  $people14PastDays ? $people14PastDays : 0?>
														</td>
													</tr>
													<tr>
														<td>
														Past 30 Days
														</td>
														
														<td>

														<?=  $people30PastDays ? $people30PastDays : 0?>
														</td>
													</tr>
												</table>
												
												
												
											</div>
										
										
										
										</div> <!-- end panel-darkblue-->
									</div> <!-- end col-md-4-->
									
									
									
									
									
									
			</div> <!-- end row-->
		
			
			

										


					