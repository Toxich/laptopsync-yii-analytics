<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use app\models\Projects;
use app\models\ProjectSettings;
use app\models\UserSettings;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;

$admin_user_id = Yii::$app->user->getId();

			
?>


<div style="max-width: 950px; margin: auto">
	
	<h1 class="page-title-h1" style="text-align: center">Projects</h1>


<div class="explainer" style="text-align: center">
Welcome back! Choose a project you want to work on.
</div>

<div class="panel panel-darkblue" id="variation-01" style="padding-left: 16px; padding-right: 16px; padding-bottom: 20px;">
			
			
			
			
			<div class="panel-body" style="width: 100%">
				                                    
													
													
								   					<div class="modal" id="create-project-menu">
								   					  <div class="modal-background"></div>
								   					  <div class="modal-content" style="background-color: #3a3b43; color: rgba(225,235,245,.95);">
								   					    <!-- Any other Bulma elements you want -->
						
								   						<div class="modal-content-padding" style="padding: 30px; text-align: center">
								   					
												<?php
													
												/*
												
												
													<?php
														
														$model = new Projects();
														
													?>
													
												    <?php $form = ActiveForm::begin(); ?>

												    <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'placeholder' => 'My Cool Website Title']) ?>

												    <?= $form->field($model, 'domain')->textInput(['placeholder' => 'https://']) ?>

												    <div class="form-group">
												        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
												    </div>

												    <?php ActiveForm::end(); ?>
												*/
													
												?>
													
													
													
												
								   						</div>
						
								   					  </div>
								   					  <button class="modal-close is-large" aria-label="close"></button>
								   					</div>
													
													
													
										
										
						<div class="columns">
													
													
										            <?php
										            	
													$superadmin_id = UserSettings::superadmin_id();	
							
													$usersettings = UserSettings::findOne(['user_id' => $admin_user_id]);
							
													if ($usersettings->project_id && $usersettings->team_permissions == 3)
													{
														// means only have access to this one project, is just a team member, cant create new projects
								
														$projects = Projects::find()->where(['user_id' => $superadmin_id, 'to_delete' => 0, 'id' => $usersettings->project_id])->all();
								
							
													}
													else
													{
														$projects = Projects::find()->where(['user_id' => $superadmin_id, 'to_delete' => 0])->all();
														
														?>
														
														
														
																<div class="single-funnel-content column" style="height: 340px; border-radius: 0px; display: inline-block; border-radius: 12px; box-sizing: border-box; overflow: hidden; border: 6px dashed #4e4f52;" id="create-new-project">
																
																
														
											                
																
																
															  <div class="panel-body panel" style="padding: 20px; background-color: #27282a; height: 340px;">
         												   	
															
																						   
															   <div class="center" style='width: 80%;'>
														   
														   															   
																	   <div style="font-size: 80px; font-weight: normal; top: -5px; position: relative;">
																		   &#65291;
																	   </div>
															   
																	   <div style="font-size: 24px; font-weight: bold; color: #FFF; margin-bottom: 10px; top: -20px; position: relative;">NEW PROJECT</div>
														   
														 
															  
														<div style=' top: -20px; position: relative;'>Consider each project a different website or brand. Every project is connected to its own domain or subdomain.</div>
															   
															   														  
															   </div>
													   
													   
								   					
													
													
														   
		   	   
																</div> <!-- end panel-body panel -->
							
							
															</div> <!-- end single-funnel-content column -->
															
															
														<?php
							
													}
													
										            ?>
													
													
													
													
											
													
													
													
				                                    
											
													
													
																													
															
								<?php
									
							
								
								$x = 0;
								
								foreach ($projects as $project)
								{
									
										
									
									$project_domain = "";
									
									
									$projectsettings = ProjectSettings::find()->where(['project_id' => $project->id])->one();
									
									if ($projectsettings)
									{
										if ($projectsettings->domain)
										{
											$project_domain = $projectsettings->domain;
										}
										elseif ($projectsettings->username)
										{
											$project_domain = "";
										}
									}
									
									if ($projectsettings)
									{
										$project_title = $projectsettings->username;
									
									}
									else
									{
										$project_title = $project->title;
									}
									
									
									
									?>
									
									
									
										<div class="single-funnel-content column" style="min-height: 340px; border-radius: 0px; display: inline-block; border-radius: 12px; overflow: hidden;" id="project-<?= $project->id ?>">
																
																
										
											                    
																
																
									     <div class="panel-body panel" style="padding: 20px; background-color: #27282a; height: 100%; background-size: cover;
											 
											 
											background: linear-gradient(
											 		      rgba( 22, 22, 22, 0.5 ), rgba(22, 22, 22, 0.5)
											 		    ),

											 		    url('<?= $project->screenshot ?>') no-repeat center center">
         												   	
															
																		   <div>
																			   
															
															
															<?php
																
																
															if ($usersettings->team_permissions == 1)
															{
																?>
																
																<ul style='position: absolute; top: 10px; right: 20px; font-size: 32px;' class='ellipses-dropdown'>
																<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cog" style="font-size: 45%; position: relative; left: 8px"></i></a>
																<ul class="dropdown-menu inner-dropdown-menu">
																	<li style='position: relative; left: -6px'><a href="<?= Url::to(['project-settings/onboarding', 'project_id' => $project->id])?>"><i class="fas fa-edit" style='font-size: 80%; position: relative; top: -1px'></i> Rename</a></li>
	
																	<li style='position: relative; left: -6px'><a href="<?= Url::to(['project-settings/update', 'project_id' => $project->id])?>"><i class="fas fa-cog" style='font-size: 80%; position: relative; top: -1px'></i> Settings</a></li>
								
					
																<li style='position: relative; left: -6px'><a href="<?= Url::to(['projects/remove', 'id' => $project->id ]) ?>" onclick="return confirm('Are you sure?');"><i class="fas fa-times"></i> Delete</a></li>
																</ul>
																</li>
																</ul>
																
																<?php
															}
															?>
															
															
																			   
																			
								<div class="center" style="width: 100%; padding: 20px; text-align: center;">
								
					
								
								
								
								
								
								
													             
	<span style="font-size: 32px; font-weight: bold; text-transform: uppercase"><?= $project_title ?></span>
																			   
											<br />
<span style="font-size: 13px; text-transform: uppercase; color: #ccc"><?= $project_domain ?></span>						
																		   
																		   
							<br /><br />
																											   
<button class='button is-primary' style='border: 0px; cursor: pointer;' id='select-<?= $project->id ?>'>
select project
</button>
																							  
																							  			   
																		   	</div>
																	   	  
																		 
																		  
																		  
																		  
																		  </div>
													   
																		   
													<script>
														$(function() {
														    $('.confirm1').click(function() {
														        return window.confirm("Are you sure?");
														    });
														});
													</script>	   
		   	   
										</div> <!-- end panel-body panel -->
							
							
									</div>
									
				
									
									
									<script>
										$("#select-<?= $project->id ?>").click(function () {
								
											window.location.href = '<?= Url::to(['funnels/index', 'project_id' => $project->id]) ?>';	
										});
									</script>
									
									
									<?php
									
									if ($x % 2 == 0)
									{
										?>
									</div>
									
									<div class="columns">
										<?php
									}
									$x++;
									
									
									
								}
									
									
								
								
								
								
								?> <!-- end single-funnel-content column -->
							
							
			
															
															
							</div> <!-- end columns-->
																	
				  
				  
				  
				      </div> <!-- end panel body -->
										
										
				</div> <!-- end panel dark blue-->
										
										<script>
											
											$( document ).ready(function() {
	
				
		
														
				
				
											$(".main-button").click(function () {
			
												console.log('in here');
											    $(this).addClass('is-loading');
											           setTimeout(function () {
											               $(this).removeClass('is-loading');
											           }, 2000);
				
											});
			
											$("#create-new-project").click(function () {
			
												//console.log('in here 2');
											    //$("#create-project-menu").addClass('is-active');
												
												window.location.href = '<?= Url::to(['projects/new']) ?>';	
												
			          
				
											});
			
			
											$(".modal-close").click(function () {
				
												$(".main-button").removeClass('is-loading');
											    $(".modal").removeClass('is-active');
			     
				 
											});
											
										});
										</script>
											
											
								
								<style>
									.nav-left {
										
									}
									
									.page-sidebar {
										display: none;
									}
									
									.page-content2 {
										width: 100% !important;
									}
									
									@media screen and  (min-width: 800px)
									{
										.column {
											max-width: 47% !important;
											margin: 2% !important;
										}	
										
									}
									
									tr, table, td {
										text-align: center !important;
									}
								
								
								
									.single-funnel-content:hover {
				
										 filter: brightness(110%) !important;
										cursor: pointer !important;
									}
									
									.left-bg {
										position: fixed; left: 0px; top: 0px; height: 100%; width: 0px; background-color: #3a3b43 !important; display: none;
									}
									
								</style>
								
								
								<style>
									.inner-dropdown-menu {
										z-index: 2147483647;
										left: -80px;
										top: 50px;
										width: 125px !important;
										min-width: 0px;
									}
			
									.inner-dropdown-menu:after {
									    position: absolute;
									    top: -7px;
									    right: 12px;
									    left: auto;
									    display: inline-block !important;
									    border-right: 7px solid transparent;
									    border-bottom: 7px solid #fff;
									    border-left: 7px solid transparent;
									    content: '';
									}	
								</style>
								
										
										
</div> <!-- end projects-->