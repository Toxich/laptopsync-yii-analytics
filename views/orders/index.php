<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Orders', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'project_id',
            'admin_user_id',
            'charge_id',
            'people_id',
            // 'product_ids',
            // 'variant_ids',
            // 'product_json:ntext',
            // 'coupon_used',
            // 'name',
            // 'tracking_number',
            // 'shipping_id',
            // 'customer_id',
            // 'shipping_name',
            // 'shipping_address',
            // 'shipping_address_line2',
            // 'shipping_city',
            // 'shipping_province_state',
            // 'shipping_zipcode',
            // 'shipping_country',
            // 'phone_number',
            // 'funnel_id',
            // 'status',
            // 'date',
            // 'subtotal',
            // 'tax',
            // 'total',
            // 'fulfilled',
            // 'shipping_offer_id',
            // 'discount_offer_id',
            // 'subtraction_amount',
            // 'payments_taken',
            // 'payments_total',
            // 'payment_type',
            // 'refunded',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
