<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'project_id',
            'admin_user_id',
            'charge_id',
            'people_id',
            'product_ids',
            'variant_ids',
            'product_json:ntext',
            'coupon_used',
            'name',
            'tracking_number',
            'shipping_id',
            'customer_id',
            'shipping_name',
            'shipping_address',
            'shipping_address_line2',
            'shipping_city',
            'shipping_province_state',
            'shipping_zipcode',
            'shipping_country',
            'phone_number',
            'funnel_id',
            'status',
            'date',
            'subtotal',
            'tax',
            'total',
            'fulfilled',
            'shipping_offer_id',
            'discount_offer_id',
            'subtraction_amount',
            'payments_taken',
            'payments_total',
            'payment_type',
            'refunded',
        ],
    ]) ?>

</div>
