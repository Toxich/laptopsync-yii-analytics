<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'project_id')->textInput() ?>

    <?= $form->field($model, 'admin_user_id')->textInput() ?>

    <?= $form->field($model, 'charge_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'people_id')->textInput() ?>

    <?= $form->field($model, 'product_ids')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'variant_ids')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_json')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'coupon_used')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tracking_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shipping_id')->textInput() ?>

    <?= $form->field($model, 'customer_id')->textInput() ?>

    <?= $form->field($model, 'shipping_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shipping_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shipping_address_line2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shipping_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shipping_province_state')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shipping_zipcode')->textInput() ?>

    <?= $form->field($model, 'shipping_country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'funnel_id')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'subtotal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fulfilled')->textInput() ?>

    <?= $form->field($model, 'shipping_offer_id')->textInput() ?>

    <?= $form->field($model, 'discount_offer_id')->textInput() ?>

    <?= $form->field($model, 'subtraction_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payments_taken')->textInput() ?>

    <?= $form->field($model, 'payments_total')->textInput() ?>

    <?= $form->field($model, 'payment_type')->textInput() ?>

    <?= $form->field($model, 'refunded')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
