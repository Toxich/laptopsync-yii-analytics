<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * @var yii\web\View                   $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module           $module
 */

$this->title = Yii::t('user', 'Welcome back!');
$this->params['breadcrumbs'][] = $this->title;

$base_url = Url::to(['/']);


?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">

<center>
<img src="<?= $base_url ?>blog/wp-content/uploads/2016/11/logo_vyper_black.png" style="width: 120px" />
</center>
<br /><br />
<div class="row" >
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default" style="background-color: #EFEFEF; padding: 20px; border: 0px; border-radius: 10px">
            <div class="panel-heading" style="background-color: #EFEFEF; border: 0px; padding: 25px">
                <h3 class="panel-title" style="font-size: 28px; font-weight: 600; text-align: center"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body">
				
                <?php $form = ActiveForm::begin([
                    'id'                     => 'login-form',
                    'enableAjaxValidation'   => true,
                    'enableClientValidation' => false,
                    'validateOnBlur'         => false,
                    'validateOnType'         => false,
                    'validateOnChange'       => false,
                ]) ?>

                <?= $form->field($model, 'login', ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]) ?>

                <?= $form->field($model, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])->passwordInput()->label(Yii::t('user', 'Password') . ($module->enablePasswordRecovery ? ' (' . Html::a(Yii::t('user', 'Forgot password?'), ['/user/recovery/request'], ['tabindex' => '5']) . ')' : '')) ?>


                <?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4'])?>
				
				<style>
				
				body {
					    background: #FFFFFF
				}
				
				input {
					background-color: #FFFFFF !important;
					border-radius: 3px !important;
					padding: 25px !important;
					
				}
			
				</style>

                <?= Html::submitButton(Yii::t('user', 'SIGN IN'), ['class' => 'button is-primary', 'tabindex' => '3','style' => 'width: 100%; height: 55px']) ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
		
        <?php if ($module->enableConfirmation): ?>
            <p class="text-center">
                <?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend']) ?>
            </p>
        <?php endif ?>
        <?php if ($module->enableRegistration): ?>
            <p class="text-center">
              
				<a href="<?= $base_url ?>user/register?redirect=upgradefree">Don't have an account? Sign up!</a>
            </p>
        <?php endif ?>
		
		
		
		<?php
			
		// un comment out this stuff
		/*
		?>
        
		
		*/
		?>
        <?= Connect::widget([
            'baseAuthUrl' => ['/user/security/auth'],
        ]) ?>
    </div>
</div>

