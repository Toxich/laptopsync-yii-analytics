<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = 'Get Posts';
$this->params['breadcrumbs'][] = ['label' => 'Feeds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile(
    '@web/js/parse.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
    '@web/js/html-layouts.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
<div class="parse-posts">

    <h1><?= Html::encode($this->title) ?></h1>
    <div id="results" ></div>
    <h2 class="parse-heading">Import your posts</h2>
    <div class="parse-form">
        <div class="parse-input-content">
            <small id="urlHelp" class="text-muted parse-input-help">Url for our website or newsletter page</small>
            <input class="form-control parse-input" name="url" id="parse-url-input">
        </div>
        <button type="button" class="parse-button" onclick="parseByUrl()">Get Started</button>
    </div>

</div>