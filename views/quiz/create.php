<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Quiz */

$this->title = 'Create Quiz';
$this->params['breadcrumbs'][] = ['label' => 'Quiz', 'url' => ['create']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile(
    '@web/js/create-quiz.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>

<div class="quiz-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <form>
        <div class="form-group">
            <label for="quiz-name-input">Quiz name</label>
            <input type="text" class="form-control quiz-name-input" id="quiz-name" placeholder="Enter quiz name">
        </div>
    </form>
    <button class="btn btn-success save-quiz-button" id="create-quiz-button">Create quiz</button>
</div>