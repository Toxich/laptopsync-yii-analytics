<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Quiz */

$this->title = 'Quiz';
$this->params['breadcrumbs'][] = ['label' => 'Quiz', 'url' => ['show']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile(
    '@web/js/html-layouts.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
    '@web/js/quiz.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
<script>
    let myQuestions = <?php echo !empty($model) ?  json_encode($model->quiz_data) : null;?>;
    if (myQuestions == null) {
        window.location = '/';
    }
    let quiz_id = <?php echo $model->id ?>;
</script>

<div class="quiz-show">
    <div class="quiz-box" >
        <h1><?= Html::encode($model->name) ?></h1>
        <div id="answer-result">

        </div>
        <h4>Filled questions progress:</h4>
        <div class="progress">
            <div class="progress-bar" id="progress" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <button id="next" class="btn btn-primary" >Next Question</button>
        <button id="submit" class="btn btn-primary">Submit Quiz</button>
        <div id="results"></div>
        <div class="quiz-container">
            <div id="quiz"></div>
        </div>

    </div>

</div>
