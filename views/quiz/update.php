<?php


use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Quiz */

$this->title = 'Manage Quiz';
$this->params['breadcrumbs'][] = ['label' => 'Quiz', 'url' => ['create']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile(
    '@web/js/tinymce.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
    '@web/js/html-layouts.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
    '@web/js/manage-quiz.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$question = [];
if (!empty($model->quiz_data)) {
    $question = $model->quiz_data[0];
} else {
    $model->quiz_data = [];
}
?>
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.10.2/Sortable.min.js" referrerpolicy="origin"></script>

<script>
    let quiz_data = <?php echo json_encode($model->quiz_data) ?>;
    let questions = quiz_data.length !== 0 ? quiz_data[quiz_data.length - 1].id : 0 ;
    let quiz_name = <?php echo json_encode($model->name)?>
</script>

<div class="quiz-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <h2><?= $model->name ?></h2>
    <div class="container">
        <div class="left">
            <ul class="list-group questions-list" id="questions-list">

            </ul>
        </div>
        <div id="quiz-content" class="right">

        </div>
        <div class="tinymce-area right" id="tinymce-area">
        </div>
    </div>
    <button class="btn btn-primary" id="add-question" onclick="addQuestion()">Add question</button>
</div>
