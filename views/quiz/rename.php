<?php


use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Quiz */

$this->title = 'Rename Quiz';
$this->params['breadcrumbs'][] = ['label' => 'Quiz', 'url' => ['create']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile(
    '@web/js/rename-quiz.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
<script>
    let id = <?php echo $model->id ?>;
</script>
<div class="quiz-rename">

    <h1><?= Html::encode($this->title) ?></h1>
    <form>
        <div class="form-group">
            <label for="quiz-name-input">Quiz name</label>
            <input type="text" class="form-control quiz-name-input" id="quiz-name" placeholder="Enter quiz name"
                   value="<?php echo $model->name ?>">
        </div>
    </form>
    <button class="btn btn-success save-quiz-button" id="rename-quiz-button">Rename quiz</button>
</div>