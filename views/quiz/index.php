<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quiz list';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quiz-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Create Quiz', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
                             'dataProvider' => $dataProvider,
                             'columns' => [
                                 ['class' => 'yii\grid\SerialColumn'],

                                 'id',
                                 'name',
                                 'created_at',
                                 [
                                     'class' => 'yii\grid\ActionColumn',
                                     'template' => '{view} {update} {delete} {renameButton}',  // the default buttons + your custom button
                                     'buttons' => [
                                         'renameButton' => function ($url, $model, $key) {     // render your custom button
                                             return Html::a(
                                                 'Rename',
                                                 ['/quiz/rename', 'id' => $key],
                                                 ['class' => 'btn btn-primary']
                                             );
                                         }
                                     ]
                                 ],
                             ],
                         ]); ?>
</div>
