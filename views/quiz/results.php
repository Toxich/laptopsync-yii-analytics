<?php


use yii\helpers\Html;
use app\models\Quiz;

/* @var $this yii\web\View */
/* @var $model app\models\Quiz */

$this->title = 'Quiz Results';
$this->params['breadcrumbs'][] = ['label' => 'Quiz', 'url' => ['create']];
$this->params['breadcrumbs'][] = $this->title;
$user_results = $_SESSION['user_results'];
?>

<div class="quiz-results">
    <h1><?= Html::encode($this->title) ?></h1>
    <h2><?= Html::encode($model->name) ?>:</h2>
    <h3>Your score - <?= $user_results['correct'] . '/' . count($model->quiz_data) ?></h3>
    <div class="results">
        <ul  class="result-list" >
            <?php foreach ($user_results['user_answers'] as $key => $answer): ?>
                <li>
                    <div class="panel panel-default <?= $answer['answer'] == Quiz::getById($answer['question_id'], $model->quiz_data)['correct_answer'] ? "panel-success" : "panel-danger" ?>">
                        <div class="panel-heading ">
                            Question: <?php echo Quiz::getById($answer['question_id'], $model->quiz_data)['question'] ?>
                        </div>
                        <div class="panel-body">
                            <p>Your answer:</p><?= $answer['type'] == 'test' ? Quiz::getById($answer['answer'], Quiz::getById($answer['question_id'], $model->quiz_data)['answers'])['answer'] :  $answer['answer'] ?>
                        </div>
                        <div class="panel-footer">
                            <p>Correct answer -</p><?=  Quiz::getById($answer['question_id'], $model->quiz_data)['type'] == 'test' ? Quiz::getById(Quiz::getById($answer['question_id'], $model->quiz_data)['correct_answer'], Quiz::getById($answer['question_id'], $model->quiz_data)['answers'])['answer']  : Quiz::getById($answer['question_id'], $model->quiz_data)['correct_answer'] ?>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
