<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserSettings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'brand_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'brand_logo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gdpr_off')->textInput() ?>

    <?= $form->field($model, 'agency_id')->textInput() ?>

    <?= $form->field($model, 'agency_client_id')->textInput() ?>

    <?= $form->field($model, 'agency_permissions')->textInput() ?>

    <?= $form->field($model, 'account_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gdpr_terms')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'support_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'industry')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'employees')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email_list')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tool')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'smtp_host')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'smtp_username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'smtp_password')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'smtp_from_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'smtp_tested')->textInput() ?>

    <?= $form->field($model, 'add_terms_checkbox')->textInput() ?>

    <?= $form->field($model, 'marketing_email_checkbox_wording')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'find_out')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tracked_sales_month')->textInput() ?>

    <?= $form->field($model, 'tracked_participants_month')->textInput() ?>

    <?= $form->field($model, 'extra_participants_month')->textInput() ?>

    <?= $form->field($model, 'extra_tracked_sales_month')->textInput() ?>

    <?= $form->field($model, 'permanent_extra_participants')->textInput() ?>

    <?= $form->field($model, 'permanent_extra_sales')->textInput() ?>

    <?= $form->field($model, 'grandfathered')->textInput() ?>

    <?= $form->field($model, 'instagram')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'twitter')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pinterest')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'facebook_group')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'large_brand_image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'logo_emails')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'youtube')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reward_image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reward')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'warning')->textInput() ?>

    <?= $form->field($model, 'cancellation_reason')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
