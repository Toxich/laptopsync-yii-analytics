<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-settings-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User Settings', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'brand_name',
            'url:url',
            'brand_logo',
            // 'facebook',
            // 'gdpr_off',
            // 'agency_id',
            // 'agency_client_id',
            // 'agency_permissions',
            // 'account_type',
            // 'gdpr_terms:ntext',
            // 'support_email:email',
            // 'industry',
            // 'employees',
            // 'email_list:email',
            // 'tool',
            // 'smtp_host',
            // 'smtp_username',
            // 'smtp_password',
            // 'smtp_from_email:email',
            // 'smtp_tested',
            // 'add_terms_checkbox',
            // 'marketing_email_checkbox_wording:email',
            // 'find_out:ntext',
            // 'tracked_sales_month',
            // 'tracked_participants_month',
            // 'extra_participants_month',
            // 'extra_tracked_sales_month',
            // 'permanent_extra_participants',
            // 'permanent_extra_sales',
            // 'grandfathered',
            // 'instagram',
            // 'twitter',
            // 'pinterest',
            // 'facebook_group',
            // 'large_brand_image',
            // 'logo_emails:email',
            // 'youtube',
            // 'reward_image',
            // 'reward:ntext',
            // 'warning',
            // 'cancellation_reason:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
